[![Maintenance](https://img.shields.io/badge/Maintained%3F-yes-green.svg)](https://GitHub.com/rosharp/The-Complete-2022-Web-Development-Bootcamp/graphs/commit-activity)
[![JavaScript](https://img.shields.io/badge/--F7DF1E?logo=javascript&logoColor=000)](https://www.javascript.com/)

This are files of completed tasks from Angela Yu's Web-Development Bootcamp course, which I took at 2021/2022.
